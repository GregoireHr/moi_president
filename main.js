(function (window, document) {

    "use strict";

    var $window = $(window),
        windowWidth = $window.width(),
        windowHeight = $window.height(),

        init = function () {
            $( ".slider" ).each(function( index ) {
                var $this = $(this),
                    $slides = $this.find(".slide"),
                    slidesNbr = $slides.length,
                    $nav = $this.find(".btn"),
                    slideIdex = 0;

                $nav.on('click', function(){
                    var isNext = $(this).hasClass("btnRight");

                    if(isNext) {
                        slideIdex++;
                        if(slideIdex == slidesNbr){
                            slideIdex = 0;
                            $slides.removeClass("visible");
                        }
                        $slides.eq(slideIdex).addClass("visible");
                    } else {
                        slideIdex--;
                        if(slideIdex == -1){
                            slideIdex = slidesNbr - 1;
                            $slides.addClass("visible");
                        }
                        $slides.eq(slideIdex+1).removeClass("visible");
                    }
                });
            });

            // Share btn
            $("[data-action='share']").click(function () {
                $(".step1").addClass("step-hidden");
                $(".step2").removeClass("step-hidden");
            });

            $(document).keydown(function (e) {
                switch (e.which) {
                case 37: // left
                    $(".btnLeft").click();
                    break;

                case 39: // right
                    $(".btnRight").click();
                    break;

                default:
                    return; // exit this handler for other keys
                }
                e.preventDefault(); // prevent the default action (scroll / move caret)
            });
        };


    $(function () {
        init();

    });


})(this, this.document);
